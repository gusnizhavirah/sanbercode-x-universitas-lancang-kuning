<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Soal String</h1>

    <?php
// SOAL NO 1
$string = "PHP is never old";
echo "<h3>Soal No 1</h3>";
echo "Panjang string: " . strlen($string) . ", Jumlah kata: " . count(explode(' ', $string)) . "<br>";

$first_sentence = "Hello PHP!";
echo "Panjang string: " . strlen($first_sentence) . ", Jumlah kata: " . count(explode(' ', $first_sentence)) . "<br>";

$second_sentence = "I'm ready for the challenges";
echo "Panjang string: " . strlen($second_sentence) . ", Jumlah kata: " . count(explode(' ', $second_sentence)) . "<br>";

// SOAL NO 2
echo "<h3>Soal No 2</h3>";
$string2 = "I love PHP";
echo "<label>String: </label>\"$string2\" <br>";
$words = explode(' ', $string2);
echo "Kata pertama: " . $words[0] . "<br>";
echo "Kata kedua: " . $words[1] . "<br>";
echo "Kata Ketiga: " . $words[2] . "<br>";

// SOAL NO 3
echo "<h3>Soal No 3</h3>";
$string3 = "PHP is old but sexy!";
echo "String: \"$string3\" <br>";
echo "String modified: \"" . str_replace('sexy', 'awesome', $string3) . "\" <br>";
?>
</body>
</html>